

$json_file = Read-Host "Please enter name of json file"
$property_name = Read-Host "Please enter property what you what to change"
[int]$value = Read-Host "Please enter new value for property"

$a = Get-Location
$json = Get-Content $a\$json_file -raw | ConvertFrom-Json
$json.$property_name = $value
$json | ConvertTo-Json -depth 32| set-content $a\$json_file

