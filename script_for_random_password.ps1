Function GeneratePassword
{   
    $PasswordLength = Read-Host "Please, enter password lenght"
    $AllowedPasswordCharacters = [char[]]'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    $Regex = "(?=.*\d)(?=.*[a-z])(?=.*[A-Z])"

    do {
            $Password = ([string]($AllowedPasswordCharacters | Get-Random -Count $PasswordLength) -replace ' ')
       }    until ($Password -cmatch $Regex)
    $Password
}
GeneratePassword
