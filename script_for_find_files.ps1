$folder = Read-Host "Please enter name of folder to files"
$substring = Read-Host "Please enter name of substirng"

Get-ChildItem -Path $folder -Include *$substring* -Recurse -Force -ErrorAction SilentlyContinue | Select-Object Name, @{Name="Size_In_Kb";Expression={"{0:N0}" -f ($_.Length / 1Kb) }}

#D:\lx-powershell-scripts\files